from django.db.models import fields
from rest_framework import serializers
from .models import Post
from api import models


class PostListSerizalizer(serializers.ModelSerializer):
    class Meta:
        model = Post
        fields = ['title', 'description', 'image']
    
class PostDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        fields = ['title', 'description', 'image', 'body', 'create_at']
class PostSerializer(serializers.Serializer):
    class Meta:
        model = Post
        fields = '__all__'