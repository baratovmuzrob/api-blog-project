from api.pagination import CustomPagination
from django.core import paginator
from rest_framework import pagination
from api.models import Post
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .serializers import PostListSerizalizer, PostSerializer, PostDetailSerializer
from rest_framework import generics
from .pagination import CustomPagination


class PostList(generics.ListAPIView):
    queryset = Post.objects.all()
    serializer_class = PostListSerizalizer
    pagination_class = CustomPagination

class PostDetail(generics.RetrieveAPIView):
    queryset = Post.objects.all()
    serializer_class = PostDetailSerializer














