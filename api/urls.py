from django.urls import path
from . import views

# urlpatterns = [
#     path('', views.apiOverview, name='api-overview'),
#     path('post-create/', views.post_create, name='post-create'),
#     path('post-list/', views.post_list, name='post-list'),
#     path('post-detail/<str:pk>/', views.post_detail, name='post-detail'),
#     path('post-update/<str:pk>/', views.post_update, name='post-update'),
#     path('post_delete/<str:pk>/', views.post_delete, name='post-delete'),
# ]

urlpatterns = [
    path('', views.PostList.as_view()),
    path('post-create/', views.PostCreate.as_view()),
    path('post-detail/<int:pk>/', views.PostDetail.as_view()),
    path('post-update/<int:pk>/', views.PostUpdate.as_view()),
    path('post-delete/<int:pk>/', views.PostDelete.as_view()),
]